/**
 * Copyright (C) 2014-2015 Frosty Elk AB
 */
/* jshint -W069, -W097, -W117, -W016, -W059 */
//noinspection JSUnusedGlobalSymbols
function GetPluginSettings()
{
	return {
		"name":			"Parse FE",				// as appears in 'insert object' dialog, can be changed as long as "id" stays the same
		"id":			"ParseFE",				// this is used to identify this plugin and is saved to the project; never change it
		"version":		"1.0.2.1",					// (float in x.y format) Plugin version - C2 shows compatibility warnings based on this
		"description":	"Access Parse functions (parse.com)",
		"author":		"Frosty Elk",
		"help url":		"http://www.frostyelk.se/support",
        "cordova-plugins": "se.frostyelk.cordova.parse.plugin",
		"dependency":	"parse-1.3.1_fe_1.js",
		"category":		"Frosty Elk",				// Prefer to re-use existing categories, but you can set anything here
		"type":			"object",				// either "world" (appears in layout and is drawn), else "object"
		"rotatable":	false,					// only used when "type" is "world".  Enables an angle property on the object.
		"flags":		0						// uncomment lines to enable flags...
						| pf_singleglobal		// exists project-wide, e.g. mouse, keyboard.  "type" must be "object".
					//	| pf_texture			// object has a single texture (e.g. tiled background)
					//	| pf_position_aces		// compare/set/get x, y...
					//	| pf_size_aces			// compare/set/get width, height...
					//	| pf_angle_aces			// compare/set/get angle (recommended that "rotatable" be set to true)
					//	| pf_appearance_aces	// compare/set/get visible, opacity...
					//	| pf_tiling				// adjusts image editor features to better suit tiled images (e.g. tiled background)
					//	| pf_animations			// enables the animations system.  See 'Sprite' for usage
					//	| pf_zorder_aces		// move to top, bottom, layer...
					//  | pf_nosize				// prevent resizing in the editor
					//	| pf_effects			// allow WebGL shader effects to be added
					//  | pf_predraw			// set for any plugin which draws and is not a sprite (i.e. does not simply draw
												// a single non-tiling image the size of the object) - required for effects to work properly
	};
}

////////////////////////////////////////
// Parameter types:
// AddNumberParam(label, description [, initial_string = "0"])			// a number
// AddStringParam(label, description [, initial_string = "\"\""])		// a string
// AddAnyTypeParam(label, description [, initial_string = "0"])			// accepts either a number or string
// AddCmpParam(label, description)										// combo with equal, not equal, less, etc.
// AddComboParamOption(text)											// (repeat before "AddComboParam" to add combo items)
// AddComboParam(label, description [, initial_selection = 0])			// a dropdown list parameter
// AddObjectParam(label, description)									// a button to click and pick an object type
// AddLayerParam(label, description)									// accepts either a layer number or name (string)
// AddLayoutParam(label, description)									// a dropdown list with all project layouts
// AddKeybParam(label, description)										// a button to click and press a key (returns a VK)
// AddAnimationParam(label, description)								// a string intended to specify an animation name
// AddAudioFileParam(label, description)								// a dropdown list with all imported project audio files

////////////////////////////////////////
// Conditions

// AddCondition(id,					// any positive integer to uniquely identify this condition
//				flags,				// (see docs) cf_none, cf_trigger, cf_fake_trigger, cf_static, cf_not_invertible,
//									// cf_deprecated, cf_incompatible_with_triggers, cf_looping
//				list_name,			// appears in event wizard list
//				category,			// category in event wizard list
//				display_str,		// as appears in event sheet - use {0}, {1} for parameters and also <b></b>, <i></i>
//				description,		// appears in event wizard dialog when selected
//				script_name);		// corresponding runtime function name
AddCondition(0,	cf_trigger, "On ready", "Start up", "On ready", "Triggered when the Parse API has loaded and is ready to be used.", "onReady");
AddCondition(1,	0, "Is ready", "Start up", "Is ready", "True when the Parse API has loaded and is ready to be used.", "isReady");
// App level save
AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different Save actions.", "\"\"");
AddCondition(2, cf_trigger | cf_deprecated, "On Class data saved", "Class data", "On Class data saved for tag <b>{0}</b>", "Triggered when the 'Save data' action completes.", "onAppDataSaved");
AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different Get actions.", "\"\"");
AddCondition(3, cf_trigger | cf_deprecated, "On Class get data", "Class data", "On Class app data for tag <b>{0}</b>", "Triggered when the 'Get data' action completes.", "onAppDataGet");

// OnConnectFBSuccessful
AddCondition(4,	cf_trigger, "On connect to FB successful", "Facebook", "On connect to FB successful", "Triggered when the Parse User is synced with Facebook.", "onConnectFBSuccessful");
AddCondition(5,	cf_trigger, "On connect to FB failed", "Facebook", "On connect to FB failed", "Triggered when the Parse User failed to sync with Facebook. Not possible to save any User data!", "onConnectFBFailed");

// User level save
AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different events.", "\"\"");
AddCondition(6, cf_trigger, "On User Class data saved", "User Class data", "On User Class data saved for tag <b>{0}</b>", "Triggered when the 'Save data' action completes.", "onUserDataSaved");
AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different events.", "\"\"");
AddCondition(7, cf_trigger, "On User Class fetch data", "User Class data", "On fetch User Class data for tag <b>{0}</b>", "Triggered when the 'Fetch data' action completes.", "onUserDataFetch");

// Find
AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different events.", "\"\"");
AddCondition(8, cf_trigger, "On Query Class sucessful", "Retrieve data with Queries", "On Query successful for tag <b>{0}</b>", "Triggered when the 'Query' action completes.", "onFindQuerySuccessful");

AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different events.", "\"\"");
AddCondition(9, cf_trigger, "On Query Class error", "Retrieve data with Queries", "On Query error for tag <b>{0}</b>", "Triggered when the 'Query' action fails.", "onFindQueryFailed");

//Insert
AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different events.", "\"\"");
AddCondition(10, cf_trigger, "On Insert Class data sucessful", "Insert Class data", "On Insert data sucessful for tag <b>{0}</b>", "Triggered when the 'Insert Class data' action completes.", "onInsertSuccessful");

AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different events.", "\"\"");
AddCondition(11, cf_trigger, "On Insert Class data error", "Insert Class data", "On Insert data error for tag <b>{0}</b>", "Triggered when the 'Insert Class data' action fails.", "onInsertFailed");


//Update
AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different events.", "\"\"");
AddCondition(12, cf_trigger, "On Update Class data sucessful", "Update Class data", "On Update data sucessful for tag <b>{0}</b>", "Triggered when the 'Update Class data' action completes.", "onUpdateSuccessful");

AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different events.", "\"\"");
AddCondition(13, cf_trigger, "On Update Class data error", "Update Class data", "On Update data error for tag <b>{0}</b>", "Triggered when the 'Update Class data' action fails.", "onUpdateFailed");

AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different events.", "\"\"");
AddCondition(14, cf_trigger, "On Upload File successful", "File", "On Upload file successful for tag <b>{0}</b>", "Triggered when the Upload File action completes.", "onFileUploadSuccessful");

AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different events.", "\"\"");
AddCondition(15, cf_trigger, "On Upload File failed", "File", "On Upload file failed for tag <b>{0}</b>", "Triggered when the Upload File action fails.", "onFileUploadFailed");

// User Sign Up
AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different events.", "\"\"");
AddCondition(200, cf_trigger, "On User Sign up Succesful", "User Authentication", "On User Sign up Succesful for tag <b>{0}</b>", "Triggered when the User Sign Up action completes.", "onUserSignUpSuccessful");

AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different events.", "\"\"");
AddCondition(201, cf_trigger, "On User Sign up failed, Email already taken", "User Authentication", "On User Sign up failed, Email already taken for tag <b>{0}</b>", "Triggered when the User Sign Up action failes", "onUserSignUpFailedEmailAlreadyTaken");

AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different events.", "\"\"");
AddCondition(202, cf_trigger, "On User Sign up failed, UserId already taken", "User Authentication", "On User Sign up failed, Userid already taken for tag <b>{0}</b>", "Triggered when the User Sign Up action failes", "onUserSignUpFailedUserIdAlreadyTaken");

AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different events.", "\"\"");
AddCondition(203, cf_trigger, "On User Sign up failed, Invalid Email", "User Authentication", "On User Sign up failed, Invalid Email for tag <b>{0}</b>", "Triggered when the User Sign Up action failes", "onUserSignUpFailedInvalidEmail");

AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different events.", "\"\"");
AddCondition(205, cf_trigger, "On User Sign up failed, other reason", "User Authentication", "On User Sign up failed, other reason for tag <b>{0}</b>", "Triggered when the User Sign Up action failes.", "onUserSignUpFailedOther");

AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different events.", "\"\"");
AddCondition(210, cf_trigger, "On User Login Successful", "User Authentication", "On User Login Succesful for tag <b>{0}</b>", "Triggered when the User Login action completes.", "onUserLoginSuccessful");

AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different events.", "\"\"");
AddCondition(215, cf_trigger, "On User Login failed", "User Authentication", "On User Login failed for tag <b>{0}</b>", "Triggered when the User Login action failes.", "onUserLogonFailed");

AddCondition(220,	0, "Is User Email verified", "User Authentication", "Is the Users Email verified", "True when the user has verified the Email.", "isUserEmailVerified");
AddCondition(225,	0, "Is User Email present", "User Authentication", "Is the Users Email present", "True when the user has signed up with a valid Email.", "isUserEmailPresent");
AddCondition(230,	0, "Is User Logged in", "User Authentication", "Is the User logged in", "True when the user is logged in.", "isUserLoggedIn");

AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different events.", "\"\"");
AddCondition(240, cf_trigger, "On User logout Successful", "User Authentication", "On User Logout Successful for tag <b>{0}</b>", "Triggered when the User Logout action completes.", "onUserLogoutSuccessful");

AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different events.", "\"\"");
AddCondition(245, cf_trigger, "On User Logout failed", "User Authentication", "On User Logout failed for tag <b>{0}</b>", "Triggered when the User Logout action failes.", "onUserLogoutFailed");

AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different events.", "\"\"");
AddCondition(250, cf_trigger, "On User reset password Successful", "User Authentication", "On User reset password Successful for tag <b>{0}</b>", "Triggered when the reset password action completes.", "onUserResetPasswordSuccessful");

AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different events.", "\"\"");
AddCondition(255, cf_trigger, "On User reset password failed", "User Authentication", "On User reset password failed for tag <b>{0}</b>", "Triggered when the User reset password action failes.", "onUserResetPasswordFailed");

AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different events.", "\"\"");
AddCondition(300, cf_trigger, "On Delete row successful", "Delete Class Data", "On Delete row successful for tag <b>{0}</b>", "Triggered when the delete row action completes.", "onDeleteRowSuccessful");

AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different events.", "\"\"");
AddCondition(310, cf_trigger, "On Delete row failed", "Delete Class Data", "On Delete row failed for tag <b>{0}</b>", "Triggered when the delete row action fails.", "onDeleteRowFailed");

AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different events.", "\"\"");
AddCondition(320, cf_trigger, "On Delete File successful", "File", "On Delete File successful for tag <b>{0}</b>", "Triggered when the delete File action completes.", "onDeleteFileSuccessful");

AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different events.", "\"\"");
AddCondition(330, cf_trigger, "On Delete File failed", "File", "On Delete File failed for tag <b>{0}</b>", "Triggered when the delete File action fails.", "onDeleteFileFailed");

AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different events.", "\"\"");
AddCondition(340, cf_trigger, "On Run Cloud Code successful", "Cloud Code", "On Run Cloud Code successful for tag <b>{0}</b>", "Triggered when the Run Cloud Code is successful.", "onRunCloudCodeSuccessful");

AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different events.", "\"\"");
AddCondition(350, cf_trigger, "On Run Cloud Code failed", "Cloud Code", "On Run Cloud Code failed for tag <b>{0}</b>", "Triggered when the Run Cloud Code action fails.", "onRunCloudCodeFailed");

// Push
AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different events.", "\"\"");
AddCondition(400, cf_trigger, "On Subscribe to channel successful", "Push", "On Subscribe to channel successful for tag <b>{0}</b>", "Triggered when the Subscribe to channel is successful.", "onPushSubscribeChannelSuccessful");

AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different events.", "\"\"");
AddCondition(405, cf_trigger, "On Unsubscribe to channel successful", "Push", "On Unsubscribe to channel successful for tag <b>{0}</b>", "Triggered when the Unsubscribe to channel is successful.", "onPushUnSubscribeChannelSuccessful");


AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different events.", "\"\"");
AddCondition(410, cf_trigger, "On Subscribe to channel failed", "Push", "On Subscribe to channel failed for tag <b>{0}</b>", "Triggered when the Subscribe to channel failed.", "onPushSubscribeChannelFailed");

AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different events.", "\"\"");
AddCondition(415, cf_trigger, "On Unsubscribe to channel failed", "Push", "On Unsubscribe to channel failed for tag <b>{0}</b>", "Triggered when the Unsubscribe to channel failed.", "onPushUnSubscribeChannelFailed");

AddCondition(420, cf_trigger, "On Push Recived", "Push", "On Push Received", "Triggered when the Push Subscription is recevied.", "onPushReceived");



////////////////////////////////////////
// Actions

// AddAction(id,				// any positive integer to uniquely identify this action
//			 flags,				// (see docs) af_none, af_deprecated
//			 list_name,			// appears in event wizard list
//			 category,			// category in event wizard list
//			 display_str,		// as appears in event sheet - use {0}, {1} for parameters and also <b></b>, <i></i>
//			 description,		// appears in event wizard dialog when selected
//			 script_name);		// corresponding runtime function name

AddStringParam("Class", "Enter a Class name.");
AddStringParam("Column", "Enter an Column name.");
AddStringParam("Value", "Enter the string to store.");
AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different events.", "\"\"");
AddAction(0, af_none | af_deprecated, "Save Class data", "Class data", "Save data <i>{1}</i>:<i>{2}</i> in Class <i>{0}</i>. Tag: <b>{3}</b> ", "Save a Class data object to Parse.", "SaveAppDataObject");

AddStringParam("Class", "Enter a Class name.");
AddStringParam("ObjectId", "Enter a data object id.");
AddStringParam("Column", "Enter an Column name.");
AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different events.", "\"\"");
AddAction(1, af_none | af_deprecated , "Get Class data", "Class data", "Get Class data from Column <i>{2}</i> from object <i>{1}</i> in Class <i>{0}</i>. Tag: <b>{3}</b> ", "Get a Class data object stored in Parse.", "GetAppDataObject");

AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different events.", "\"\"");
AddAction(2, af_none, "Save and sync User Class data", "User Class data", "Save and sync User Class data. Tag: <b>{0}</b> ", "Save and sync User Class data. Normally done once at end of layout", "SaveUserDataObject");

AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different events.", "\"\"");
AddAction(3, af_none, "Fetch User Class data", "User Class data", "Fetch and sync the User Class from Parse. Tag: <b>{0}</b> ", "Fetch the User Class stored in Parse. Used after e.g Sign up to get current email status", "FetchUserDataObject");

AddAction(4, af_none, "Connect to Facebook", "User authentication", "Connect the Facebook User to the Parse User", "Connect to Facebook via the FacebookFE plugin. Needed to store User Class data.", "ConnectFB");

AddStringParam("Column", "Enter an Column name.");
AddAction(5, af_none | af_deprecated , "Get User Class data", "User Class data", "Get User data for Column <i>{0}</i>", "Get User Class data.", "GetUserDataAttribute");

AddStringParam("Column", "Enter an Column name.");
AddAnyTypeParam("Value","Enter the value to store.", "\"\"");
AddAction(8, af_none, "Set User Class data", "User Class data", "Set User Class data to <i>{1}</i> for Column <i>{0}</i> ", "Set User Class data.", "setUserClassColumnData");

// Queries
AddStringParam("Class", "Enter a Class name to Query on.");
AddAction(9, af_none, "Start a Query build for a Class", "Retrieve data with Queries", "Start a Query build for Class <i>{0}</i>.", "Start Query builder.", "StartQuery");

AddStringParam("Column", "Enter an Column name.");
AddAnyTypeParam("Equal to Value", "Enter a value to filter on.");
AddAction(10, af_none, "Add 'Equal to' filter", "Filter data", "Add Filter 'Equal to' <i>{1}</i> for Column <i>{0}</i> .", "Add 'Equal to' filter.", "EqualToQuery");

AddComboParamOption("Parse data");
AddComboParamOption("C2 Array");
AddComboParamOption("Index Loop");
AddComboParam("Return result as", "The way to get the result. Array/Parse result is in LastData/QueryData. Loop will trigger once for each row with info variables loaded.");
AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different events.", "\"\"");
AddAction(11, af_none, "Find data", "Retrieve data with Queries", "Execute a Query Find with result as <i>{0}</i>, return in event tag <b>{1}</b>.", "Find data.", "FindQuery");

AddNumberParam("Limit Find result to", "Enter a limit for data to retrive.");
AddAction(12, af_none, "Add 'Limit' filter", "Filter data", "Add Filter 'Limit' to <i>{0}</i> row(s) in Query.", "Add 'Limit' filter.", "LimitQuery");

AddStringParam("Column", "Enter an Column name.");
AddAnyTypeParam("Not Equal to Value", "Enter a value to filter on.");
AddAction(13, af_none, "Add 'Not Equal to' filter", "Filter data", "Add Filter 'Not Equal to' <i>{1}</i> for Column <i>{0}</i>.", "Add 'Not Equal to' filter to query.", "NotEqualToQuery");

AddStringParam("Column", "Enter an Column name.");
AddNumberParam("Greater Than Value", "Enter a value to filter on.");
AddAction(14, af_none, "Add 'Greater Than' filter", "Filter data", "Add Filter 'Greater Than' <i>{1}</i> for Column <i>{0}</i>.", "Add 'Greater Than' filter to query.", "GreaterThanQuery");

AddStringParam("Column", "Enter an Column name.");
AddNumberParam("Less Than Value", "Enter a value to filter on.");
AddAction(15, af_none, "Add 'Less Than' filter", "Filter data", "Add Filter 'Less Than' <i>{1}</i> for Column <i>{0}</i>.", "Add 'Less Than' filter to query.", "LessThanQuery");

AddNumberParam("Skip rows", "Enter a number of rows to skip.");
AddAction(16, af_none, "Add 'Skip' filter", "Filter data", "Add Filter 'Skip' to <i>{0}</i> row(s) in Query.", "Add 'Skip' filter.", "SkipQuery");

AddStringParam("Column", "Enter the Column name.");
AddAnyTypeParam("Value","Enter the value to update.");
AddAction(17, af_none, "Prepare to update Class data", "Update Class data", "Prepare to update Column <i>{0}</i> to value <i>{1}</i> ", "Prepare to update Class data.", "prepareToUpdateData");

AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different events.", "\"\"");
AddAction(18, af_none, "Update and save data", "Update Class data", "Update and save the prepared data, return in event tag <b>{0}</b>.", "Update and save data.", "updateAndSave");

AddStringParam("Class", "Enter a Class name to Insert data into.");
AddAction(19, af_none, "Start Insert Build for a Class", "Insert Class data", "Start Insert Build for Class <i>{0}</i>.", "Start Insert Build for a Class.", "startInsert");

AddStringParam("Column", "Enter the Column name.");
AddAnyTypeParam("Value","Enter the value to insert.");
AddAction(20, af_none, "Prepare to insert Class data", "Insert Class data", "Prepare to insert value <i>{1}</i> into Column <i>{0}</i>.", "Prepare to Insert Class data.", "prepareToInsertData");

AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different events.", "\"\"");
AddAction(21, af_none, "Insert and save Data", "Insert Class data", "Insert and save the prepared data, return in event tag <b>{0}</b>.", "Insert and save data.", "insertAndSave");

AddNumberParam("Index","Enter the query index to update.", 0);
AddAction(22, af_none, "Start an Update build", "Update Class data", "Start an Update build for query index <i>{0}</i> ", "Start an Update build.", "startUpdateData");

AddStringParam("Columns", "Enter Columns to select.");
AddAction(23, af_none, "Select Column(s) to retrieve", "Filter data", "Select Column(s) <i>{0}</i>.", "Select Column(s) to retrieve.", "selectQuery");

AddStringParam("Column", "Enter Columns for ascending sort .");
AddAction(24, af_none, "Sort data ascending", "Sort data", "Sort ascending on Column <i>{0}</i>.", "Sort ascending on Column.", "ascending");

AddStringParam("Column", "Enter Columns for descending sort .");
AddAction(25, af_none, "Sort data descending", "Sort data", "Sort descending on Column <i>{0}</i>.", "Sort descending on Column.", "descending");

AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different events.", "\"\"");
AddAction(27, af_none, "Count Rows", "Retrieve data with Queries", "Execute a Query Count, return in event tag <b>{0}</b>.", "Count Rows in a Class.", "countQuery");

AddStringParam("File name", "File name. Then set encoding as one of Binary: e.g AJAX loaded images, ByteArray: [0xBE, 0xEF, 0xCA], Base64: Pre encoded Base64 file, Web File: From Web File upload control.");
AddComboParamOption("Binary");
AddComboParamOption("Byte Array");
AddComboParamOption("Base64");
AddComboParamOption("Web File");
AddComboParam("File Encoding", "Binary: e.g AJAX loaded images, ByteArray: [0xBE, 0xEF, 0xCA], Base64: Pre encoded Base64 file, Web File: From Web File upload control. ");
AddAnyTypeParam("Content data","Enter the file data to upload. Max 10 MB");
AddStringParam("Optional Content Type", "Enter the Content-Type (optional)");
AddAction(28, af_none, "Create File Object", "File", "Create a File Object <i>{0}</i>, encoded as <i>{1}</i>. Optional Content-Type: <i>{3}</i>.", "Create File Object.", "createFileObject");

AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different events.", "\"\"");
AddAction(29, af_none, "Upload File", "File", "Upload Parse File, return in event tag <b>{0}</b>.", "Upload file.", "uploadFile");

AddStringParam("Column", "Enter the Column name.");
AddAction(30, af_none, "Prepare to insert File object", "File", "Prepare to the insert current File object into Column <i>{0}</i>.", "Prepare to insert File object.", "prepareToInsertFileObject");

AddStringParam("Column", "Enter the Column name.");
AddAction(31, af_none, "Prepare to update File object", "File", "Prepare to the update current File object for Column <i>{0}</i>.", "Prepare to update File object.", "prepareToUpdateFileObject");

AddStringParam("Column", "Enter the Column name.");
AddAnyTypeParam("Index","Enter the query index.", 0);
AddAction(32, af_none, "Get the File object", "File", "Get the File object from Column <i>{0}</i> index <i>{1}</i> and set as current File Object.", "Get the File Object from a Class Column and set as the current File object.", "getFileObject");

AddStringParam("Column", "Enter the Column name.");
AddAction(33, af_none, "Add File object to User Class", "User Class data", "Add current File object into Column <i>{0}</i> for the User Class.", "Add current File object into a Column for the User Class.", "addFileObjectToUser");

AddStringParam("Column", "Enter the Column name.");
AddAction(34, af_none, "Get the File object from User Class", "User Class data", "Get the File object from Column <i>{0}</i> in User Class and set as current File Object.", "Get the File Object from a User Class Column and set as the current File object.", "getFileObjectFromUser");

AddStringParam("File name", "The file extension is used to decide file type, .png, .jpg etc");
AddObjectParam("Object", "Choose the object.");
AddAction(35, af_none, "Create File Object from Sprite", "File", "Create a File Object <i>{0}</i> from Sprite <i>{1}</i>.", "Create a File Object from Sprite.", "createFileObjectFromSprite");


// Parse User Authentication
AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different events.", "\"\"");
AddStringParam("Userid", "The userid, this should be unique in the database");
AddStringParam("Password", "The password");
AddStringParam("Email", "The Email, this should be unique in the database");
AddAction(200, af_none, "Sign up the user with Parse", "User authentication", "Sign up userid <i>{1}</i> with password <i>{2}</i> and email <i>{3}</i>. Tag: <b>{0}</b> ", "Sign up the user to Parse.", "signUpUser");

AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different events.", "\"\"");
AddStringParam("Userid", "The userid");
AddStringParam("Password", "The password");
AddAction(210, af_none, "Login the user to Parse", "User authentication", "Login userid <i>{1}</i> with password <i>{2}</i>. Tag: <b>{0}</b> ", "Login the user to Parse.", "loginUser");

AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different events.", "\"\"");
AddAction(220, af_none, "Logout from Parse", "User authentication", "Logout from Parse. Tag: <b>{0}</b> ", "Logout from Parse.", "logoutUser");

AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different events.", "\"\"");
AddStringParam("Email", "The Email used to send out a reset password mail");
AddAction(230, af_none, "Reset the user password", "User authentication", "Send a reset password Email for <i>{1}</i>. Tag: <b>{0}</b> ", "Send a reset password email to the user.", "resetUserPassword");

// Delete row
AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different events.", "\"\"");
AddNumberParam("Index","Enter the query index to delete.", 0);
AddAction(300, af_none, "Delete a row", "Delete Class data", "Delete the row with query index <i>{1}</i>, return in event tag <b>{0}</b>.", "Insert and save data.", "deleteRowIndex");

// Delete file
AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different events.", "\"\"");
AddStringParam("File URL", "The File URL to delete");
AddAction(310, af_none, "Delete File", "File", "Delete the File <i>{1}</i>, return in event tag <b>{0}</b>.", "Delete a file from storage.", "deleteFile");

// Cloud Code
AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different events.", "\"\"");
AddStringParam("Function Name", "Enter the Cloud Code function name.");
AddStringParam("Parameter Key 1", "Enter a Parameter Key.");
AddAnyTypeParam("Parameter Value 1","Enter a Parameter Value.", "");
AddStringParam("Parameter Key 2", "Enter a Parameter Key.");
AddAnyTypeParam("Parameter Value 2","Enter a Parameter Value.", "");
AddStringParam("Parameter Key 3", "Enter a Parameter Key.");
AddAnyTypeParam("Parameter Value 3","Enter a Parameter Value.", "");
AddStringParam("Parameter Key 4", "Enter a Parameter Key.");
AddAnyTypeParam("Parameter Value 4","Enter a Parameter Value.", "");
AddAction(350, af_none, "Run Cloud code", "Cloud Code", "Call Cloud function <i>{1}</i>, return in event tag <b>{0}</b>", "Call a Cloud function", "runCloudCode");

// Analytics
AddStringParam("Event Name", "Enter the Analytics Event Name.");
AddStringParam("Key 1", "Enter a Dimension Key.");
AddStringParam("Value 1","Enter a Dimension Value.", "");
AddStringParam("Key 2", "Enter a Dimension Key.");
AddStringParam("Value 2","Enter a Dimension Value.", "");
AddStringParam("Key 3", "Enter a Dimension Key.");
AddStringParam("Value 3","Enter a Dimension Value.", "");
AddStringParam("Key 4", "Enter a Dimension Key.");
AddStringParam("Value 4","Enter a Dimension Value.", "");
AddStringParam("Key 5", "Enter a Dimension Key.");
AddStringParam("Value 5","Enter a Dimension Value.", "");
AddStringParam("Key 6", "Enter a Dimension Key.");
AddStringParam("Value 6","Enter a Dimension Value.", "");
AddStringParam("Key 7", "Enter a Dimension Key.");
AddStringParam("Value 7","Enter a Dimension Value.", "");
AddStringParam("Key 8", "Enter a Dimension Key.");
AddStringParam("Value 8","Enter a Dimension Value.", "");
AddAction(400, af_none, "Analytics track event", "Analytics", "Analytics event <i>{0}</i>.", "Track an event with Parse Analytics.", "trackAnalytics");

// Filters
AddStringParam("Column", "Enter an Column name.");
AddStringParam("Contains any of", "Enter an list of values contained in the column.", "\"\"");
AddStringParam("Value delimiter", "Enter the delimiter to separate values in the list.", "\",\"");
AddAction(500, af_none, "Add 'Contained in' filter", "Filter data", "Add Filter 'Contained in' <i>{1}</i> for Column <i>{0}</i> .", "Add 'Contained in' filter.", "containedIn");

AddStringParam("Column", "Enter an Column name.");
AddStringParam("Contains any of", "Enter a JSON formatted array of values.", "\"[]\"");
AddAction(501, af_none | af_deprecated, "Add 'Contained in' filter (JSON)", "Filter data", "Add Filter 'Contained in' <i>{1}</i> for Column <i>{0}</i> .", "Add 'Contained in' filter.", "containedInJSON");

AddStringParam("Column", "Enter an Column name.");
AddStringParam("Contains none of", "Enter an list of values not contained in the column.", "\"\"");
AddStringParam("Value delimiter", "Enter the delimiter to separate values in the list.", "\",\"");
AddAction(510, af_none, "Add 'Not Contained in' filter", "Filter data", "Add Filter 'Not Contained in' <i>{1}</i> for Column <i>{0}</i> .", "Add 'Not Contained in' filter.", "notContainedIn");


AddStringParam("Column", "Enter an Column name.","\"\"");
AddStringParam("Starts with string", "Enter a value to filter on.");
AddAction(600, af_none, "Add 'Starts with' filter", "Filter data", "Add Filter 'Starts with' <i>{1}</i> for Column <i>{0}</i> .", "Add 'Starts with' filter.", "startsWith");


AddStringParam("Column", "Enter an Column name.");
AddNumberParam("Less than or equal to value", "Enter a value to filter on.");
AddAction(610, af_none, "Add 'Less Than Or Equal To' filter", "Filter data", "Add Filter 'Less Than Or Equal To' <i>{1}</i> for Column <i>{0}</i>.", "Add 'Less Than Or Equal To' filter to query.", "lessThanOrEqualTo");


AddStringParam("Column", "Enter an Column name.");
AddNumberParam("Greater than or equal to value", "Enter a value to filter on.");
AddAction(620, af_none, "Add 'Greater Than Or Equal To' filter", "Filter data", "Add Filter 'Greater Than Or Equal To' <i>{1}</i> for Column <i>{0}</i>.", "Add 'Greater Than Or Equal To' filter to query.", "greaterThanOrEqualTo");


// Push
AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different events.", "\"\"");
AddStringParam("Channel","Enter the channel to subscribe to.", "");
AddAction(700, af_none, "Subscribe to Channel", "Push", "Subscribe to channel <i>{1}</i>, return in event tag <b>{0}</b>.", "Subscribe to channel.", "pushSubscribeChannel");

AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different events.", "\"\"");
AddStringParam("Channel","Enter the channel to unsubscribe to.", "");
AddAction(710, af_none, "Unsubscribe to Channel", "Push", "Unsubscribe to channel <i>{1}</i>, return in event tag <b>{0}</b>.", "Unsubscribe to channel.", "pushUnSubscribeChannel");



////////////////////////////////////////
// Expressions

// AddExpression(id,			// any positive integer to uniquely identify this expression
//				 flags,			// (see docs) ef_none, ef_deprecated, ef_return_number, ef_return_string,
//								// ef_return_any, ef_variadic_parameters (one return flag must be specified)
//				 list_name,		// currently ignored, but set as if appeared in event wizard
//				 category,		// category in expressions panel
//				 exp_name,		// the expression name after the dot, e.g. "foo" for "myobject.foo" - also the runtime function name
//				 description);	// description in expressions panel
AddExpression(0, ef_return_string, "Get last data", "Common event information", "LastData", "Get the data returned by the last successful request. As JSON string");
AddExpression(1, ef_return_string, "Get last object id", "Common event information", "LastObjectId", "Get the data object id from the last successful save data action.");
AddExpression(2, ef_return_string, "Get last error", "Common event information", "LastError", "Get the error from the last failed action.");

AddStringParam("Column", "Column", "\"\"");
AddExpression(3, ef_return_any | ef_deprecated, "Get User Class Column data", "Deprecated", "UserAttribute", "Get the User Class Columns data from the current Parse User");

AddNumberParam("Index", "Index", 0);
AddStringParam("Column", "Column", "\"\"");
AddExpression(4, ef_return_any, "Get Query data", "Query data", "QueryData", "Get the Column value from the last Query Find");

AddExpression(5, ef_return_number, "Get Query Rows", "Queries", "QueryRowCount", "Get the number of returned rows from the last Query");
AddExpression(6, ef_return_number, "Get Query Index", "Queries", "QueryDataIndex", "Get the curent row index in the Query Find loop");

AddStringParam("Column", "Column", "\"\"");
AddExpression(7, ef_return_any, "Get Current Query data", "Query data", "QueryDataCurrent", "Get the Column value from the current row in Query Find loop");

AddStringParam("Column", "Column", "\"\"");
AddExpression(8, ef_return_any, "Get User Class Column data", "User Class data", "UserColumnData", "Get the User Class Column data from the current Parse User");

AddExpression(9, ef_return_any, "Get File Object Name", "File", "FileObjectName", "Get the File Object Name from the current File Object");
AddExpression(10, ef_return_any, "Get File Object URL", "File", "FileObjectUrl", "Get the File Object URL from the current File Object");

//  Push
AddExpression(15, ef_return_any, "Get Push Installation Id", "Push", "PushInstallationId", "Get the Push Installation Id");

AddStringParam("Key", "´Key", "\"\"");
AddExpression(20, ef_return_any  | ef_deprecated, "Get Push data Value", "Push", "PushDataValue", "Get a Push data Value");

AddExpression(30, ef_return_any, "Get Push data as JSON", "Push", "PushDataJSON", "Get the Push data as JSON string");
AddExpression(40, ef_return_string, "Get Push Alert message", "Push", "PushDataAlert", "Get the Push Alert message");


// @Deprecated
AddExpression(999, ef_return_number | ef_deprecated, "Get Query Rows", "Deprecated", "QueryDataLength", "Get the number of returned rows from the last Query Find");


////////////////////////////////////////
ACESDone();

////////////////////////////////////////
// Array of property grid properties for this plugin
// new cr.Property(ept_integer,		name,	initial_value,	description)		// an integer value
// new cr.Property(ept_float,		name,	initial_value,	description)		// a float value
// new cr.Property(ept_text,		name,	initial_value,	description)		// a string
// new cr.Property(ept_color,		name,	initial_value,	description)		// a color dropdown
// new cr.Property(ept_font,		name,	"Arial,-16", 	description)		// a font with the given face name and size
// new cr.Property(ept_combo,		name,	"Item 1",		description, "Item 1|Item 2|Item 3")	// a dropdown list (initial_value is string of initially selected item)
// new cr.Property(ept_link,		name,	link_text,		description, "firstonly")		// has no associated value; simply calls "OnPropertyChanged" on click

var property_list = [
	new cr.Property(ept_text, "Application Id", "", "Parse Application Id"),
	new cr.Property(ept_text, "Javascript Key", "", "Parse Javascript Key"),
	new cr.Property(ept_text, "Client Key", "", "Parse Client Key. Used in Cordova plugin (Push)")
	];

// Called by IDE when a new object type is to be created
//noinspection JSUnusedGlobalSymbols
function CreateIDEObjectType() {
	return new IDEObjectType();
}

// Class representing an object type in the IDE
function IDEObjectType() {
	assert2(this instanceof arguments.callee, "Constructor called as a function");
}

// Called by IDE when a new object instance of this type is to be created
//noinspection JSUnusedGlobalSymbols
IDEObjectType.prototype.CreateInstance = function (instance) {
	return new IDEInstance(instance);
};

// Class representing an individual instance of an object in the IDE
function IDEInstance(instance, type) {
	assert2(this instanceof arguments.callee, "Constructor called as a function");

	// Save the constructor parameters
	//noinspection JSUnusedGlobalSymbols
	this.instance = instance;
	this.type = type;

	// Set the default property values from the property table
	this.properties = {};

	for (var i = 0; i < property_list.length; i++) {
		this.properties[property_list[i].name] = property_list[i].initial_value;
	}

	// Plugin-specific variables
	// this.myValue = 0...
}

// Called when inserted via Insert Object Dialog for the first time
//noinspection JSUnusedGlobalSymbols
IDEInstance.prototype.OnInserted = function () {};

// Called when double clicked in layout
//noinspection JSUnusedGlobalSymbols
IDEInstance.prototype.OnDoubleClicked = function () {};

// Called after a property has been changed in the properties bar
//noinspection JSUnusedGlobalSymbols
IDEInstance.prototype.OnPropertyChanged = function (property_name) {};

// For rendered objects to load fonts or textures
//noinspection JSUnusedGlobalSymbols
IDEInstance.prototype.OnRendererInit = function (renderer) {};

// Called to draw self in the editor if a layout object
//noinspection JSUnusedGlobalSymbols
IDEInstance.prototype.Draw = function (renderer) {};

// For rendered objects to release fonts or textures
//noinspection JSUnusedGlobalSymbols
IDEInstance.prototype.OnRendererReleased = function (renderer) {};