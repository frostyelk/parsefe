/**
 * Copyright (C) 2014-2015 Frosty Elk AB
 */
/* jshint -W069, -W097, -W117 */
/*global Parse, ParsePlugin, FB */
// ECMAScript 5 strict mode
"use strict";

assert2(cr, "cr namespace not created");
assert2(cr.plugins_, "cr.plugins_ not created");

/////////////////////////////////////
// Plugin class
cr.plugins_.ParseFE = function (runtime) {
    this.runtime = runtime;
};

//noinspection UnterminatedStatementJS
(function () {


    var pluginProto = cr.plugins_.ParseFE.prototype;

    /////////////////////////////////////
    // Object type class
    pluginProto.Type = function (plugin) {
        this.plugin = plugin;
        this.runtime = this.plugin.runtime;
    };

    var typeProto = pluginProto.Type.prototype;

    // Plugin common vars
    var parseId = "";
    var parseKey = "";
    var parseClientKey = "";
    var parseReady = false;
    var parseInst = null;
    var parseRuntime = null;
    var parseConditionTag = "";
    var parseLastData = "";
    var parseLastError = "";
    var parseResultObjId = "";
    var parseCurrentObject = {};
    // var parseScriptUrl = "//www.parsecdn.com/js/parse-1.2.19.js";
    // var parseScriptUrl = "//www.parsecdn.com/js/parse-1.2.19.min.js";
    //var parseScriptUrl = "parse-1.3.1_fe_1.js";
    var conditionTag = {};

    var queryObject = {};
    var queryData = [];
    var queryDataIndex = 0;
    var queryRowCount = 0;

    var updateObject = {};
    var updateIndex = 0;

    var insertObject = {};

    var fileObject = {};

    var parseCordovaPluginReady = false;
    var parseInstallationId = "";
    var parsePushData = {};
    var parsePushDataAlert = "";
    var parsePushDataJSON = {};
    var parsePushDataBackground = false;

    // called on startup for each object type
    typeProto.onCreate = function () {
    };

    /////////////////////////////////////
    // Instance class
    pluginProto.Instance = function (type) {
        this.type = type;
        this.runtime = this.type.runtime;
    };

    var instanceProto = pluginProto.Instance.prototype;

    // called whenever an instance is created
    instanceProto.onCreate = function () {


        parseInst = this;
        parseRuntime = this.runtime;
        parseId = this.properties[0];
        parseKey = this.properties[1];
        parseClientKey = this.properties[2];

        if (parseId !== "" && parseKey !== "" && typeof(Parse) !== 'undefined') {
            // jQuery["getScript"](parseScriptUrl, function() { // use this for cdn loading
            Parse["initialize"](parseId, parseKey);
            // console.log("Parse API loaded and initialized!");

            // Check for Push Cordova plugin
            if (parseRuntime.isPhoneGap && typeof window["ParsePlugin"] !== 'undefined') {

                // Entry point for the Push on the JS-level
                document.addEventListener('onParsePushReceived', function (pushBundle) {
                    // pushBundle is of type Event for Android

                    var foreground = pushBundle["foreground"] ? pushBundle["foreground"] : false;
                    var coldstart = pushBundle["coldstart"] ? pushBundle["coldstart"] : false;

                    if (parseRuntime.isiOS) {
                        // Load data
                        parsePushData = pushBundle["aps"] || {};
                        parsePushDataAlert = parsePushData["alert"] || "";

                        // The JSON data should be present at the root level
                        parsePushDataJSON = pushBundle;

                        // Remove root level plugin internal information
                        delete parsePushDataJSON["coldstart"];
                        delete parsePushDataJSON["foreground"];

                        // iOS Starts and set the app into foreground automatically when in background
                        // Resume for iOS is called before 'onParsePushReceived'
                        // So no need to wait for resume
                        if (coldstart) {
                            console.log("coldstart in iOS, set push in background true");
                            parsePushDataBackground = true;
                        } else {
                            console.log("warmstart in iOS, we should be in foreground now");
                            parsePushDataBackground = false;
                            parseRuntime.trigger(cr.plugins_.ParseFE.prototype.cnds.onPushReceived, parseInst);
                        }

                    } else if (parseRuntime.isAndroid) {
                        // Load data
                        parsePushData = pushBundle["payload"] || {};
                        var pushInternalData = parsePushData["com.parse.Data"] || {};
                        parsePushDataAlert = pushInternalData["alert"] || "";
                        // The JSON data should be present at the root level
                        parsePushDataJSON = pushInternalData;

                        // Execute specific actions for Android
                        // At the moment the actions are the same as for iOS
                        // but...
                        if (coldstart) {
                            console.log("Android coldstart");
                            parsePushDataBackground = true;
                        } else {
                            console.log("Android warmstart");
                            parsePushDataBackground = false;
                            parseRuntime.trigger(cr.plugins_.ParseFE.prototype.cnds.onPushReceived, parseInst);
                        }

                    } else {
                        // Other platforms
                        console.warn("Unsupported Push platform");
                    }

                    // Construct2 r195 is quicker to start layouts.
                    // Check already here for cold start events.
                    if (parseRuntime.running_layout && parsePushDataBackground) {
                        parsePushDataBackground = false;
                        parseRuntime.trigger(cr.plugins_.ParseFE.prototype.cnds.onPushReceived, parseInst);
                    }

                    console.log("onParsePushReceived, parsePushDataJSON: " + JSON.stringify(parsePushDataJSON));
                });

                document.addEventListener('resume', function () {
                    console.log("ParseFE resume: ");
                    // Not used
                });

                ParsePlugin["initialize"](parseId, parseClientKey, function (result) {
                    parseCordovaPluginReady = true;
                    console.log("Parse Push is ready: " + result);

                    ParsePlugin["getInstallationId"](function (id) {
                        parseInstallationId = id;
                        console.log("Parse Installation Id: " + "****" /* + parseInstallationId*/);

                        ParsePlugin["getPendingPush"](function (result) {
                            console.log("Parse getPendingPush: " + result);
                        });

                    }, function (error) {
                        console.log("Failed to get Installation Id: " + error);
                    });

                }, function (error) {
                    parseCordovaPluginReady = false;
                    console.log("Parse Push is Not ready: " + error);
                });


            }


            // Delay OnReady event so layout is loaded before trying to do events
            parseRuntime.tickMe(parseInst);


            // }); // cdn
        } else {
            console.log("Parse API not setup");
        }
    };

    instanceProto.tick = function () {
        // Just do this once after load
        if (parseRuntime.running_layout) {
            parseRuntime.untickMe(parseInst);
            parseReady = true;
            parseRuntime.trigger(cr.plugins_.ParseFE.prototype.cnds.onReady, parseInst);

            if (parsePushDataBackground) {
                parsePushDataBackground = false;
                console.log("Sending push after restart: " + JSON.stringify(parsePushData));
                parseRuntime.trigger(cr.plugins_.ParseFE.prototype.cnds.onPushReceived, parseInst);
            }
        }
    };


    // The comments around these functions ensure they are removed when exporting, since the
    // debugger code is no longer relevant after publishing.
    /**BEGIN-PREVIEWONLY**/
    instanceProto.getDebuggerValues = function (propsections) {
        // Append to propsections any debugger sections you want to appear.
        // Each section is an object with two members: "title" and "properties".
        // "properties" is an array of individual debugger properties to display
        // with their name and value, and some other optional settings.
        propsections.push({
            "title": "Parse data information",
            "properties": [
                // Each property entry can use the following values:
                // "name" (required): name of the property (must be unique within this section)
                // "value" (required): a boolean, number or string for the value
                // "html" (optional, default false): set to true to interpret the name and value
                //									 as HTML strings rather than simple plain text
                // "readonly" (optional, default false): set to true to disable editing the property

                // Example:
                {"name": "Query Row Count", "value": queryRowCount},
                {"name": "Query Data Index", "value": queryDataIndex},
                {"name": "Last Object Id", "value": parseResultObjId},
                {"name": "Last Error", "value": parseLastError},
                {"name": "Last Data", "value": parseLastData}
            ]
        });
    };

    instanceProto.onDebugValueEdited = function (header, name, value) {
        // Called when a non-readonly property has been edited in the debugger. Usually you only
        // will need 'name' (the property name) and 'value', but you can also use 'header' (the
        // header title for the section) to distinguish properties with the same name.
        /*
         if (name === "My property")
         this.myProperty = value;
         */
    };
    /**END-PREVIEWONLY**/

    //////////////////////////////////////
    // Conditions
    function Cnds() {
    }

    Cnds.prototype.isReady = function () {
        return parseReady;
    };

    Cnds.prototype.onReady = function () {
        return true;
    };

    // @Deprecated
    Cnds.prototype.onAppDataSaved = function (tag) {
        return cr.equals_nocase(tag, parseConditionTag);
    };

    // @Deprecated
    Cnds.prototype.onAppDataGet = function (tag) {
        return cr.equals_nocase(tag, parseConditionTag);
    };

    Cnds.prototype.onUserDataSaved = function (tag) {
        return cr.equals_nocase(tag, conditionTag.saveuserdata);
    };

    Cnds.prototype.onUserDataFetch = function (tag) {
        return cr.equals_nocase(tag, conditionTag.fetchuserdata);
    };

    Cnds.prototype.onConnectFBSuccessful = function () {
        return true;
    };

    Cnds.prototype.onConnectFBFailed = function () {
        return true;
    };

    Cnds.prototype.onFindQuerySuccessful = function (tag) {
        return cr.equals_nocase(tag, conditionTag.query);
    };

    Cnds.prototype.onFindQueryFailed = function (tag) {
        return cr.equals_nocase(tag, conditionTag.query);
    };

    Cnds.prototype.onInsertSuccessful = function (tag) {
        return cr.equals_nocase(tag, conditionTag.insert);
    };

    Cnds.prototype.onInsertFailed = function (tag) {
        return cr.equals_nocase(tag, conditionTag.insert);
    };


    Cnds.prototype.onUpdateSuccessful = function (tag) {
        return cr.equals_nocase(tag, conditionTag.update);
    };

    Cnds.prototype.onUpdateFailed = function (tag) {
        return cr.equals_nocase(tag, conditionTag.update);
    };


    Cnds.prototype.onFileUploadSuccessful = function (tag) {
        return cr.equals_nocase(tag, conditionTag.uploadfile);
    };

    Cnds.prototype.onFileUploadFailed = function (tag) {
        return cr.equals_nocase(tag, conditionTag.uploadfile);
    };

    Cnds.prototype.onUserSignUpSuccessful = function (tag) {
        return cr.equals_nocase(tag, conditionTag.signup);
    };

    Cnds.prototype.onUserSignUpFailedUserIdAlreadyTaken = function (tag) {
        return cr.equals_nocase(tag, conditionTag.signup);
    };

    Cnds.prototype.onUserSignUpFailedEmailAlreadyTaken = function (tag) {
        return cr.equals_nocase(tag, conditionTag.signup);
    };

    Cnds.prototype.onUserSignUpFailedInvalidEmail = function (tag) {
        return cr.equals_nocase(tag, conditionTag.signup);
    };


    Cnds.prototype.onUserSignUpFailedOther = function (tag) {
        return cr.equals_nocase(tag, conditionTag.signup);
    };

    Cnds.prototype.onUserLoginSuccessful = function (tag) {
        return cr.equals_nocase(tag, conditionTag.login);
    };

    Cnds.prototype.onUserLogonFailed = function (tag) {
        return cr.equals_nocase(tag, conditionTag.login);
    };

    Cnds.prototype.onUserLogoutSuccessful = function (tag) {
        return cr.equals_nocase(tag, conditionTag.logout);
    };

    Cnds.prototype.onUserLogoutFailed = function (tag) {
        return cr.equals_nocase(tag, conditionTag.logout);
    };


    Cnds.prototype.isUserLoggedIn = function () {
        return Parse["User"]["current"]() ? true : false;
    };

    Cnds.prototype.isUserEmailVerified = function () {
        var user = Parse["User"]["current"]();
        var verified = user ? user["get"]("emailVerified") : null;
        return verified ? verified : false;
    };

    Cnds.prototype.isUserEmailPresent = function () {
        var user = Parse["User"]["current"]();
        var emailPresent = user ? user["get"]("emailVerified") : false;

        return typeof emailPresent === 'undefined' ? false : emailPresent;
    };

    Cnds.prototype.onUserResetPasswordSuccessful = function (tag) {
        return cr.equals_nocase(tag, conditionTag.reset);
    };

    Cnds.prototype.onUserResetPasswordFailed = function (tag) {
        return cr.equals_nocase(tag, conditionTag.reset);
    };


    Cnds.prototype.onDeleteRowSuccessful = function (tag) {
        return cr.equals_nocase(tag, conditionTag.delete);
    };

    Cnds.prototype.onDeleteRowFailed = function (tag) {
        return cr.equals_nocase(tag, conditionTag.delete);
    };


    Cnds.prototype.onDeleteFileSuccessful = function (tag) {
        return cr.equals_nocase(tag, conditionTag.deleteFile);
    };

    Cnds.prototype.onDeleteFileFailed = function (tag) {
        return cr.equals_nocase(tag, conditionTag.deleteFile);
    };

    Cnds.prototype.onRunCloudCodeSuccessful = function (tag) {
        return cr.equals_nocase(tag, conditionTag.cloud);
    };


    Cnds.prototype.onRunCloudCodeFailed = function (tag) {
        return cr.equals_nocase(tag, conditionTag.cloud);
    };


    Cnds.prototype.onPushSubscribeChannelSuccessful = function (tag) {
        return cr.equals_nocase(tag, conditionTag.pushSubscribe);
    };

    Cnds.prototype.onPushSubscribeChannelFailed = function (tag) {
        return cr.equals_nocase(tag, conditionTag.pushSubscribe);
    };

    Cnds.prototype.onPushUnSubscribeChannelSuccessful = function (tag) {
        return cr.equals_nocase(tag, conditionTag.pushUnSubscribe);
    };

    Cnds.prototype.onPushUnSubscribeChannelFailed = function (tag) {
        return cr.equals_nocase(tag, conditionTag.pushUnSubscribe);
    };

    Cnds.prototype.onPushReceived = function () {
        return true;
    };

    pluginProto.cnds = new Cnds();


    //////////////////////////////////////
    // Actions
    function Acts() {
    }

    // @Deprecated
    Acts.prototype.SaveAppDataObject = function (class_, attribute_, value_, tag_) {

        parseConditionTag = tag_;

        parseLastData = "";
        parseResultObjId = "";
        parseLastError = "";

        var ClassObject = Parse["Object"]["extend"](class_);
        var dataObject = new ClassObject();

        var storeObject = {};
        storeObject[attribute_] = value_;

        parseCurrentObject = dataObject;
        dataObject["set"](storeObject);
        dataObject["save"](null, {
            success: function (dataObject) {
                parseResultObjId = dataObject["id"];
                parseLastData = JSON.stringify(parseResultObjId);
                parseRuntime.trigger(cr.plugins_.ParseFE.prototype.cnds.onAppDataSaved, parseInst);
            },
            error: function (dataObject, error) {
                parseLastError = JSON.stringify(error);
                console.log("Failed to save data, error object: " + parseLastError);
            }
        });
    };

    // @Deprecated
    Acts.prototype.GetAppDataObject = function (class_, objectId_, attribute_, tag_) {

        parseConditionTag = tag_;

        parseLastData = "";
        parseResultObjId = "";
        parseLastError = "";

        var ClassObject = Parse["Object"]["extend"](class_);
        var query = new Parse["Query"](ClassObject);

        query["get"](objectId_, {
            success: function (dataObject) {
                parseLastData = dataObject.get(attribute_);
                parseResultObjId = dataObject["id"];

                parseRuntime.trigger(cr.plugins_.ParseFE.prototype.cnds.onAppDataGet, parseInst);
            },
            error: function (dataObject, error) {
                parseLastError = JSON.stringify(error);
                console.log("Failed to get data, error object: " + parseLastError);
            }
        });
    };


    Acts.prototype.ConnectFB = function () {
        // Guardians
        if (typeof (FB) === 'undefined') {
            return;
        }

        parseLastData = "";

        //console.log("Calling FB.init() from Parse plugin.");
        Parse["FacebookUtils"]["init"]({});

        // Start with a fresh Parse User. Use the user from the FB-login
        Parse["User"]["logOut"]();

        var FBauthResponse = FB["getAuthResponse"]();

        // All FB auth must be handled outside the Parse plugin
        if (FBauthResponse) {
            // Build new authData for Parse
            var authData = {
                "id": FBauthResponse["userID"],
                "access_token": FBauthResponse["accessToken"],
                "expiration_date": new Date(FBauthResponse["expiresIn"] * 1000 +
                (new Date()).getTime()).toJSON()
            };

            //console.log("Calling FB.login() from Parse plugin.");
            Parse["FacebookUtils"]["logIn"](authData, {
                success: function (user) {
                    parseLastData = JSON.stringify(user);

                    // An extra check just to be really sure
                    if (Parse["User"]["current"]()) {
                        parseRuntime.trigger(cr.plugins_.ParseFE.prototype.cnds.onConnectFBSuccessful, parseInst);
                    } else {
                        parseRuntime.trigger(cr.plugins_.ParseFE.prototype.cnds.onConnectFBFailed, parseInst);
                    }
                },
                error: function (user, error) {
                    console.log("Parse.FacebookUtils.logIn. User: " + JSON.stringify(user));
                    console.log("Parse.FacebookUtils.logIn. Error: " + JSON.stringify(error));
                    parseLastData = JSON.stringify(user);
                    parseLastError = JSON.stringify(error);
                    parseRuntime.trigger(cr.plugins_.ParseFE.prototype.cnds.onConnectFBFailed, parseInst);
                }
            });

        } else {
            parseLastError = "Need to be logged in to FB before Parse does a FB connect";
            console.log(parseLastError);
            parseRuntime.trigger(cr.plugins_.ParseFE.prototype.cnds.onConnectFBFailed, parseInst);
        }


    };

    Acts.prototype.SaveUserDataObject = function (tag_) {
        if (!Parse["User"]["current"]()) {
            console.warn("Saving user data without a Parse User. Need to login first");
            return;
        }

        conditionTag.saveuserdata = "";

        parseLastError = "";

        var user = Parse["User"]["current"]();

        user["save"](null, {
            success: function (dataObject) {
                parseResultObjId = dataObject["id"] ? dataObject["id"] : "";
                conditionTag.saveuserdata = tag_;
                parseRuntime.trigger(cr.plugins_.ParseFE.prototype.cnds.onUserDataSaved, parseInst);
            },
            error: function (dataObject, error) {
                parseLastError = JSON.stringify(error);
                console.log("Failed to save user data, error object: " + parseLastError);
            }
        });
    };

    Acts.prototype.FetchUserDataObject = function (tag_) {
        if (!Parse["User"]["current"]()) {
            console.warn("Call to FetchUserDataObject without a Parse User. Need to login first!");
            return;
        }

        conditionTag.fetchuserdata = "";

        parseLastError = "";

        var user = Parse["User"]["current"]();

        user["fetch"]({
            success: function (dataObject) {
                parseResultObjId = dataObject["id"] ? dataObject["id"] : "";
                parseLastData = JSON.stringify(user);
                conditionTag.fetchuserdata = tag_;
                parseRuntime.trigger(cr.plugins_.ParseFE.prototype.cnds.onUserDataFetch, parseInst);
            },
            error: function (dataObject, error) {
                parseLastError = JSON.stringify(error);
                console.log("Failed to get user data, error object: " + parseLastError);
            }
        });
    };

    // @Deprecated
    Acts.prototype.GetUserDataAttribute = function (attribute_) {
        if (!Parse["User"]["current"]()) {
            console.warn("Call to GetUserDataAttribute without a Parse User. Need to login first!");
            return;
        }

        var user = Parse["User"]["current"]();
        parseLastData = user["get"](attribute_);

        // Set on return object?

    };


    Acts.prototype.setUserClassColumnData = function (attribute_, value_) {
        if (!Parse["User"]["current"]()) {
            console.warn("Setting User Class data without a Parse User. Need to login first!");
            return;
        }

        var user = Parse["User"]["current"]();

        var storeObject = {};
        storeObject[attribute_] = value_;

        user["set"](storeObject);
    };

    /////////////////////////   Queries
    Acts.prototype.StartQuery = function (qClass) {
//		console.log("Starting query for : " + qClass);
        var queryClass = Parse["Object"]["extend"](qClass);
        queryObject = new Parse["Query"](queryClass);
    };


    Acts.prototype.EqualToQuery = function (qAttribute, qValue) {
        queryObject["equalTo"](qAttribute, qValue);
    };


    Acts.prototype.lessThanOrEqualTo = function (qAttribute, qValue) {
        queryObject["lessThanOrEqualTo"](qAttribute, qValue);
    };

    Acts.prototype.greaterThanOrEqualTo = function (qAttribute, qValue) {
        queryObject["greaterThanOrEqualTo"](qAttribute, qValue);
    };

    Acts.prototype.startsWith = function (qAttribute, qValue) {
        queryObject["startsWith"](qAttribute, qValue);
    };


    // Return the correct type from a string
    // TODO: Correctly return float
    function parseStringToType(stringType) {
        if (isNaN(stringType)) {
            return stringType;
        } else {
            return parseInt(stringType);
        }
    }

    function parseArrayToType(stringArray) {
        var returnArray = [];

        for (var x = 0; x < stringArray.length; x++) {
            returnArray[x] = parseStringToType(stringArray[x]);
        }
        return returnArray;
    }


    Acts.prototype.containedIn = function (qAttribute, qValue, delimiter) {
        console.log("Contained in Value: " + qValue);

        var stringArray = qValue.split(delimiter);

        // Final Array
        var searchArray = parseArrayToType(stringArray);
        console.dir(searchArray);

        queryObject["containedIn"](qAttribute, searchArray);
    };


    Acts.prototype.notContainedIn = function (qAttribute, qValue, delimiter) {
        console.log("Not Contained in Value: " + qValue);

        var stringArray = qValue.split(delimiter);

        // Final Array
        var searchArray = parseArrayToType(stringArray);
        console.dir(searchArray);

        queryObject["notContainedIn"](qAttribute, searchArray);
    };


    // Test, input problems for the JSON String
    Acts.prototype.containedInJSON = function (qAttribute, qValue) {
        console.log("Contained in Value: " + qValue);

        var jsonArray = JSON.parse(qValue);

        // JSON errors
        if (jsonArray === 'undefined') {
            console.warn("ContainedIn JSON Error, not valid JSON: " + jsonArray);
            return;
        }

        console.dir(jsonArray);

        queryObject["containedIn"](qAttribute, jsonArray);
    };


    Acts.prototype.NotEqualToQuery = function (qAttribute, qValue) {
        queryObject["notEqualTo"](qAttribute, qValue);
    };

    Acts.prototype.GreaterThanQuery = function (qAttribute, qValue) {
        queryObject["greaterThan"](qAttribute, qValue);
    };

    Acts.prototype.ascending = function (qColumn) {
        queryObject["ascending"](qColumn);
    };

    Acts.prototype.descending = function (qColumn) {
        queryObject["descending"](qColumn);
    };

    Acts.prototype.SkipQuery = function (qValue) {
        queryObject["skip"](qValue);
    };


    Acts.prototype.LessThanQuery = function (qAttribute, qValue) {
        queryObject["lessThan"](qAttribute, qValue);
    };


    Acts.prototype.LimitQuery = function (qValue) {
        queryObject["limit"](qValue);
    };

    // TODO: implement this for more than one column
    Acts.prototype.selectQuery = function (qValue) {
//		console.log("Select value orig: " + qValue);
//		var variadicStringParameter = qValue.split();
//		console.log("Select value split: " + JSON.stringify(variadicStringParameter));
        queryObject["select"](qValue);
    };


    Acts.prototype.FindQuery = function (resultAs, tag) {
//		console.log("Starting FindQuery: " + tag);
        conditionTag.query = "";

        if (resultAs > 2) {
            console.warn("Error in Find Query tag for type");
            return;
        }

        var executeResult = [];

        // Parse
        executeResult[0] = function (results) {
//			console.log("Result data(Parse) as JSON: " + JSON.stringify(results));
            parseLastData = JSON.stringify(results);
            parseRuntime.trigger(cr.plugins_.ParseFE.prototype.cnds.onFindQuerySuccessful, parseInst);
        };


        // Array
        executeResult[1] = function (results) {
//			console.log("Result data(array) as JSON: " + JSON.stringify(results));

            var arr = results;
            var lengthY = 0;
            var lengthZ = 0;

            var arrResult = [];
            var len = arr.length;
            var resultIdx = 0;

            for (var i = 0; i < len; i++) {
                arrResult[resultIdx] = [];
                arrResult[resultIdx][0] = [];
                arrResult[resultIdx][0][0] = arr[i];
                resultIdx++;
            }

            // If there is something in the array the y and z need to be 1
            if (arrResult.length === 0) {
                lengthY = 0;
                lengthZ = 0;
            } else {
                lengthY = 1;
                lengthZ = 1;
            }

            parseLastData = JSON.stringify({
                "c2array": true,
                "size": [arrResult.length, lengthY, lengthZ],
                "data": arrResult
            });

            parseRuntime.trigger(cr.plugins_.ParseFE.prototype.cnds.onFindQuerySuccessful, parseInst);

        };

        // Loop
        executeResult[2] = function (results) {
//			console.log("Result data(loop) as JSON: " + JSON.stringify(results));
            var len = results.length;

            for (var l = 0; l < len; l++) {
                parseLastData = JSON.stringify(results[l]);
                queryDataIndex = l;
                parseRuntime.trigger(cr.plugins_.ParseFE.prototype.cnds.onFindQuerySuccessful, parseInst);
            }
        };

        queryObject["find"]({
            success: function (results) {
                queryData = results;
                queryRowCount = queryData.length;
                console.log("Successfully retrieved data with Query Find. Rows: " + queryRowCount);

                conditionTag.query = tag;
                executeResult[resultAs](results);
            },
            error: function (error) {
                parseLastError = JSON.stringify(error);
                console.log("Failed to find query data, error object: " + parseLastError);

                conditionTag.query = tag;
                parseRuntime.trigger(cr.plugins_.ParseFE.prototype.cnds.onFindQueryFailed, parseInst);
            }
        });
    };


    Acts.prototype.countQuery = function (tag) {
//		console.log("Starting countQuery: " + tag);
        conditionTag.query = "";

        queryObject["count"]({
            success: function (count) {
                console.log("Successfully retrieved number of rows with Query Count: " + count);
                queryRowCount = count;
                conditionTag.query = tag;
                parseRuntime.trigger(cr.plugins_.ParseFE.prototype.cnds.onFindQuerySuccessful, parseInst);
            },
            error: function (error) {
                parseLastError = JSON.stringify(error);
                console.log("Failed to query count, error object: " + parseLastError);
                conditionTag.query = tag;
                parseRuntime.trigger(cr.plugins_.ParseFE.prototype.cnds.onFindQueryFailed, parseInst);
            }
        });
    };


    Acts.prototype.startUpdateData = function (index) {
//		console.log("startUpdateData() index: " + index);
        updateIndex = index;

        if (queryData.length === 0 || index > queryData.length - 1) {
            parseLastError = "Trying to update data out range index";
            console.warn("Trying to update data out range index");
        } else {
            updateObject = queryData[updateIndex];
        }
    };


    Acts.prototype.prepareToUpdateData = function (column, value) {
//		console.log("prepareToUpdateData()");
        updateObject.set(column, value);
    };


    Acts.prototype.updateAndSave = function (tag) {
//		console.log("Starting updateAndSave: " + tag);
        conditionTag.update = "";

        updateObject["save"](null, {
            success: function (dataObject) {
                parseResultObjId = dataObject["id"] ? dataObject["id"] : "";
                parseLastData = JSON.stringify(parseResultObjId);
                console.log("Successfully updated data. Object Id: " + parseResultObjId);

                conditionTag.update = tag;
                parseRuntime.trigger(cr.plugins_.ParseFE.prototype.cnds.onUpdateSuccessful, parseInst);
            },
            error: function (dataObject, error) {
                parseLastError = JSON.stringify(error);
                console.log("Failed to update data, error object: " + parseLastError);

                conditionTag.update = tag;
                parseRuntime.trigger(cr.plugins_.ParseFE.prototype.cnds.onUpdateFailed, parseInst);
            }
        });
    };


    Acts.prototype.startInsert = function (updateClass) {
//		console.log("Starting startInsert");

        var ClassObject = Parse["Object"]["extend"](updateClass);
        insertObject = new ClassObject();

    };

    Acts.prototype.prepareToInsertData = function (column, value) {
//		console.log("Starting prepareToInsertData");

        insertObject.set(column, value);
    };


    Acts.prototype.prepareToInsertFileObject = function (column) {
        console.log("Starting prepareToInsertFileObject");

        if (fileObject instanceof Parse["File"]) {
            insertObject.set(column, fileObject);
        } else {
            console.warn("Parse File is not a Parse.File object. The file will be lost");
        }

    };


    Acts.prototype.prepareToUpdateFileObject = function (column) {
        console.log("prepareToUpdateFileObject()");

        if (fileObject instanceof Parse["File"]) {
            updateObject.set(column, fileObject);
        } else {
            console.warn("Parse File is not a Parse.File object. The file will be lost");
        }

    };


    Acts.prototype.insertAndSave = function (tag) {
//		console.log("Starting insertAndSave tag: " + tag);
        conditionTag.insert = "";

        insertObject["save"](null, {
            success: function (dataObject) {
                parseResultObjId = dataObject["id"] ? dataObject["id"] : "";
                parseLastData = JSON.stringify(parseResultObjId);
                console.log("Successfully inserted data. Object Id: " + parseResultObjId);

                conditionTag.insert = tag;
                parseRuntime.trigger(cr.plugins_.ParseFE.prototype.cnds.onInsertSuccessful, parseInst);
            },
            error: function (dataObject, error) {
                parseLastError = JSON.stringify(error);
                console.log("Failed to update data, error object: " + parseLastError);

                conditionTag.insert = tag;
                parseRuntime.trigger(cr.plugins_.ParseFE.prototype.cnds.onInsertFailed, parseInst);
            }
        });
    };


    Acts.prototype.createFileObject = function (fileName, encoding, fileContent, fileType) {
        //console.log("Starting createFileObject");

        console.log("typeof fileContent: " + typeof fileContent);

        var encodedFileData;

        // If file type is null, Parse will make a guess from file name extension
        fileType = fileType === "" ? null : fileType;

        var ENCODING_BINARY = 0;
        var ENCODING_BYTEARRAY = 1;
        var ENCODING_BASE64 = 2;
        var ENCODING_WEBFILE = 3;

        switch (encoding) {
            case ENCODING_BINARY:
                var len = fileContent.length;
                encodedFileData = [];
                if (typeof fileContent === 'string') {
                    console.log("Binary input as String");
                    encodedFileData = fileContent.split("");
//				console.log("File content length: " + fileContent.length);
//				console.log("Encoded File data length: " + encodedFileData.length);
//				for (var y = 0; y < len; y++) {
//					if (y < 50) {
//						console.log("fileContent[" + y + "]: " + fileContent[y]);
//						console.log("encodedFileData[" + y + "]: " + encodedFileData[y]);
//					}
//				}

                } else {
                    console.log("Binary input as other");
                    for (var x = 0; x < len; x++) {
                        encodedFileData.push(fileContent[x]);
                        if (x < 50) {
                            console.log("fileContent[" + x + "]: " + fileContent[x]);
                            console.log("encodedFileData[" + x + "]: " + encodedFileData[x]);
                        }
                    }
                }

                break;

            case ENCODING_BYTEARRAY:
                if (!(fileContent instanceof Array)) {
                    console.warn("Parse input file encoding is not Array");
                    break;
                }
                encodedFileData = fileContent;
                break;


            case ENCODING_BASE64:
                if (!fileContent["base64"]) {
                    console.warn("Parse input file encoding is not Base64");
                }
                encodedFileData = fileContent;
                break;

            case ENCODING_WEBFILE:
                if (!(fileContent instanceof File)) {
                    console.warn("Parse input file encoding is not File");
                }
                encodedFileData = fileContent;
                break;
        }

        fileObject = new Parse["File"](fileName, encodedFileData, fileType);

//		console.log("File data object: " + JSON.stringify(fileObject));

    };


    Acts.prototype.uploadFile = function (tag) {
        conditionTag.uploadfile = "";

        fileObject["save"]().then(function (response) {
            parseResultObjId = response["id"] ? response["id"] : "";
            // response is a Parse.File object
            // url(), name()
            console.log("File save successful");

            conditionTag.uploadfile = tag;
            parseRuntime.trigger(cr.plugins_.ParseFE.prototype.cnds.onFileUploadSuccessful, parseInst);
        }, function (error) {
            console.log("File save error: " + JSON.stringify(error));
            parseLastError = JSON.stringify(error);

            conditionTag.uploadfile = tag;
            parseRuntime.trigger(cr.plugins_.ParseFE.prototype.cnds.onFileUploadFailed, parseInst);
        });

    };

    Acts.prototype.getFileObject = function (column, idx) {
        if (queryData.length === 0 || idx > queryData.length - 1) {
            console.warn("QueryData index overflow: " + idx);
            return;
        }

        fileObject = queryData[idx].get(column);

        if (!fileObject instanceof Parse["File"]) {
            console.warn("Getting a File file object that is not a Parse.File object. File object operations will fail");
        }

    };


    Acts.prototype.addFileObjectToUser = function (column) {
        console.log("Starting addFileObjectToUser");

        var user = Parse["User"]["current"]();

        if (!user) {
            console.warn("Setting User Class data without a Parse User. Need to connect to Facebook first!");
            return;
        }

        if (fileObject instanceof Parse["File"]) {
            user["set"](column, fileObject);
        } else {
            console.warn("Parse File is not a Parse.File object. The file will be lost");
        }
    };


    Acts.prototype.getFileObjectFromUser = function (column) {
        console.log("Starting getFileObjectFromUser");

        var user = Parse["User"]["current"]();
        if (user) {
            fileObject = user["get"](column);

            if (!fileObject instanceof Parse["File"]) {
                console.warn("Getting a File file object that is not a Parse.File object. File object operations will fail");
            }
        } else {
            console.warn("Trying to get Parse User Data without a Parse User. Need to Authenticate user first!");
        }
    };


    Acts.prototype.createFileObjectFromSprite = function (fileName, gameObject) {
        //console.log("createFileObjectFromSprite");

        // Get the current instance of the object
        var sol = gameObject.getCurrentSol();
        var instances = sol.getObjects();
        var obj = instances[0];

        if (!obj) {
            console.log("No instance of '" + gameObject.name + "' is available to create a File object from");
            return;
        }

        //noinspection JSUnresolvedVariable
        var image = obj.curFrame.texture_img;

        var canvasSprite = document.createElement("canvas");
        var ctxSprite = canvasSprite.getContext("2d");

        canvasSprite.width = image.width;
        canvasSprite.height = image.height;

        ctxSprite.drawImage(image, 0, 0);
        var dataURL = canvasSprite.toDataURL("image/png");
        var base64Data = {
            'base64': dataURL
        };

        fileObject = new Parse["File"](fileName, base64Data, null);

        //console.dir(fileObject);
    };


    Acts.prototype.signUpUser = function (tag, userid, password, email) {
        conditionTag.signup = "";

        var user = new Parse["User"]();
        user.set("username", userid);
        user.set("password", password);
        user.set("email", email);

        user["signUp"](null, {
            'success': function (user) {
                //console.log("Sign up success for user: " + JSON.stringify(user));
                conditionTag.signup = tag;
                parseResultObjId = user["id"] ? user["id"] : "";
                parseRuntime.trigger(cr.plugins_.ParseFE.prototype.cnds.onUserSignUpSuccessful, parseInst);
            },
            'error': function (user, error) {
                console.log("Sign up failed for user: " + JSON.stringify(user));
                console.log("Sign up failed with error: " + JSON.stringify(error));

                parseLastError = error ? JSON.stringify(error) : "Parse Internal Error, no error message recieved";

                conditionTag.signup = tag;

                if (error && error["code"] === 202) {
                    parseRuntime.trigger(cr.plugins_.ParseFE.prototype.cnds.onUserSignUpFailedUserIdAlreadyTaken, parseInst);
                } else if (error && error["code"] === 203) {
                    parseRuntime.trigger(cr.plugins_.ParseFE.prototype.cnds.onUserSignUpFailedEmailAlreadyTaken, parseInst);
                } else if (error && error["code"] === 125) {
                    parseRuntime.trigger(cr.plugins_.ParseFE.prototype.cnds.onUserSignUpFailedInvalidEmail, parseInst);
                } else {
                    parseRuntime.trigger(cr.plugins_.ParseFE.prototype.cnds.onUserSignUpFailedOther, parseInst);
                }

            }
        });

    };


    Acts.prototype.loginUser = function (tag, userid, password) {
        conditionTag.login = "";

        Parse["User"]["logIn"](userid, password, {
            'success': function (user) {
                //console.log("Login success for user: " + JSON.stringify(user));

                if (Parse["User"]["current"]()) {
                    console.log("Parse login user OK");
                    conditionTag.login = tag;
                    parseRuntime.trigger(cr.plugins_.ParseFE.prototype.cnds.onUserLoginSuccessful, parseInst);
                } else {
                    console.log("Parse login user NOK");
                    conditionTag.login = tag;
                    parseRuntime.trigger(cr.plugins_.ParseFE.prototype.cnds.onUserLogonFailed, parseInst);
                }

            },
            'error': function (user, error) {
                console.log("Login failed for user: " + JSON.stringify(user));
                console.log("Login failed with error: " + JSON.stringify(error));

                parseLastError = error ? JSON.stringify(error) : "Parse Internal Error, no error message recieved";

                conditionTag.login = tag;
                parseRuntime.trigger(cr.plugins_.ParseFE.prototype.cnds.onUserLogonFailed, parseInst);
            }
        });

    };


    Acts.prototype.logoutUser = function (tag) {
        conditionTag.logout = "";

        Parse["User"]["logOut"]();

        if (Parse["User"]["current"]()) {
            conditionTag.logout = tag;
            parseRuntime.trigger(cr.plugins_.ParseFE.prototype.cnds.onUserLogoutFailed, parseInst);
        } else {
            conditionTag.logout = tag;
            parseRuntime.trigger(cr.plugins_.ParseFE.prototype.cnds.onUserLogoutSuccessful, parseInst);
        }
    };


    Acts.prototype.resetUserPassword = function (tag, email) {
        conditionTag.reset = "";

        Parse["User"]["requestPasswordReset"](email, {
            success: function () {
                //console.log("Reset password successful");
                conditionTag.reset = tag;
                parseRuntime.trigger(cr.plugins_.ParseFE.prototype.cnds.onUserResetPasswordSuccessful, parseInst);
            },
            error: function (error) {
                console.log("Reset password failed with error: " + JSON.stringify(error));
                parseLastError = error ? JSON.stringify(error) : "Parse Internal Error, no error message recieved";
                conditionTag.reset = tag;
                parseRuntime.trigger(cr.plugins_.ParseFE.prototype.cnds.onUserResetPasswordFailed, parseInst);
            }
        });
    };


    Acts.prototype.deleteRowIndex = function (tag, index) {
        conditionTag.delete = "";

        var deleteObject = {};

        if (queryData.length === 0 || index > queryData.length - 1) {
            parseLastError = "Trying to delete a row out range index";
            console.warn("Trying to delete a row out range index");
            parseRuntime.trigger(cr.plugins_.ParseFE.prototype.cnds.onDeleteRowFailed, parseInst);
            return;
        } else {
            deleteObject = queryData[index];
        }

        deleteObject["destroy"]({
            success: function () {
                //console.log("Delete row successful");
                conditionTag.delete = tag;
                parseRuntime.trigger(cr.plugins_.ParseFE.prototype.cnds.onDeleteRowSuccessful, parseInst);
            },
            error: function (error) {
                console.log("Delete row failed with error: " + JSON.stringify(error));
                parseLastError = error ? JSON.stringify(error) : "Parse Internal Error, no error message recieved";
                conditionTag.delete = tag;
                parseRuntime.trigger(cr.plugins_.ParseFE.prototype.cnds.onDeleteRowFailed, parseInst);
            }
        });

    };


    Acts.prototype.deleteFile = function (tag, file) {
        conditionTag.deleteFile = "";

        Parse["Cloud"]["run"]('DeleteFile', {
            'Name': file
        }, {
            success: function (result) {
                //console.log("Delete File Successful with result: " + JSON.stringify(result));
                parseLastData = JSON.stringify(result);
                conditionTag.deleteFile = tag;
                parseRuntime.trigger(cr.plugins_.ParseFE.prototype.cnds.onDeleteFileSuccessful, parseInst);
            },
            error: function (error) {
                console.log("Delete File failed with error: " + JSON.stringify(error));
                parseLastError = error ? JSON.stringify(error) : "Parse Internal Error, no error message recieved";
                conditionTag.deleteFile = tag;
                parseRuntime.trigger(cr.plugins_.ParseFE.prototype.cnds.onDeleteFileFailed, parseInst);
            }
        });
    };


    Acts.prototype.trackAnalytics = function (event, k1, v1, k2, v2, k3, v3, k4, v4, k5, v5, k6, v6, k7, v7, k8, v8) {

        var dimensions = {};

        if (k1 !== "") {
            dimensions[k1] = v1;
        }
        if (k2 !== "") {
            dimensions[k2] = v2;
        }
        if (k3 !== "") {
            dimensions[k3] = v3;
        }
        if (k4 !== "") {
            dimensions[k4] = v4;
        }
        if (k5 !== "") {
            dimensions[k5] = v5;
        }
        if (k6 !== "") {
            dimensions[k6] = v6;
        }
        if (k7 !== "") {
            dimensions[k7] = v7;
        }
        if (k8 !== "") {
            dimensions[k8] = v8;
        }

        Parse["Analytics"]["track"](event, dimensions);

    };


    Acts.prototype.runCloudCode = function (tag, cloudFunction, k1, v1, k2, v2, k3, v3, k4, v4) {
        conditionTag.cloud = "";

        var parameters = {};

        if (k1 !== "") {
            parameters[k1] = v1;
        }
        if (k2 !== "") {
            parameters[k2] = v2;
        }
        if (k3 !== "") {
            parameters[k3] = v3;
        }
        if (k4 !== "") {
            parameters[k4] = v4;
        }

        Parse["Cloud"]["run"](cloudFunction, parameters, {
            success: function (result) {
                //console.log("Run Cloud code Successful with result: " + JSON.stringify(result));
                parseLastData = JSON.stringify(result);
                conditionTag.cloud = tag;
                parseRuntime.trigger(cr.plugins_.ParseFE.prototype.cnds.onRunCloudCodeSuccessful, parseInst);
            },
            error: function (error) {
                console.log("Run Cloud code failed with error: " + JSON.stringify(error));
                parseLastError = error ? JSON.stringify(error) : "Parse Internal Error, no error message recieved";
                conditionTag.cloud = tag;
                parseRuntime.trigger(cr.plugins_.ParseFE.prototype.cnds.onRunCloudCodeFailed, parseInst);
            }
        });
    };


    Acts.prototype.pushSubscribeChannel = function (tag, channel) {
        if (!parseCordovaPluginReady) {
            parseLastError = "Push plugin not ready";
            conditionTag.pushSubscribe = tag;
            parseRuntime.trigger(cr.plugins_.ParseFE.prototype.cnds.onPushSubscribeChannelFailed, parseInst);
            return;
        }

        conditionTag.pushSubscribe = "";

        ParsePlugin["subscribe"](channel, function (result) {
            parseLastData = result;
//			console.log("Push Subscribe Channel Successful with result: " + JSON.stringify(result));
            conditionTag.pushSubscribe = tag;
            parseRuntime.trigger(cr.plugins_.ParseFE.prototype.cnds.onPushSubscribeChannelSuccessful, parseInst);
        }, function (error) {
            console.log("Push Subscribe Channel failed with error: " + JSON.stringify(error));
            parseLastError = error ? JSON.stringify(error) : "Parse Internal Error, no error message recieved";
            conditionTag.pushSubscribe = tag;
            parseRuntime.trigger(cr.plugins_.ParseFE.prototype.cnds.onPushSubscribeChannelFailed, parseInst);
        });
    };


    Acts.prototype.pushUnSubscribeChannel = function (tag, channel) {
        if (!parseCordovaPluginReady) {
            parseLastError = "Push plugin not ready";
            conditionTag.pushUnSubscribe = tag;
            parseRuntime.trigger(cr.plugins_.ParseFE.prototype.cnds.onPushUnSubscribeChannelFailed, parseInst);
            return;
        }

        conditionTag.pushUnSubscribe = "";

        ParsePlugin["unsubscribe"](channel, function (result) {
            parseLastData = result;
            console.log("Push UnSubscribe Channel Successful with result: " + JSON.stringify(result));
            conditionTag.pushUnSubscribe = tag;
            parseRuntime.trigger(cr.plugins_.ParseFE.prototype.cnds.onPushUnSubscribeChannelSuccessful, parseInst);
        }, function (error) {
            console.log("Push UnSubscribe Channel failed with error: " + JSON.stringify(error));
            parseLastError = error ? JSON.stringify(error) : "Parse Internal Error, no error message recieved";
            conditionTag.pushUnSubscribe = tag;
            parseRuntime.trigger(cr.plugins_.ParseFE.prototype.cnds.onPushUnSubscribeChannelFailed, parseInst);
        });
    };


    pluginProto.acts = new Acts();


    //////////////////////////////////////
    // Expressions
    function Exps() {
    }

    Exps.prototype.LastData = function (ret) {
        ret.set_string(parseLastData);
    };


    Exps.prototype.LastError = function (ret) {
        ret.set_string(parseLastError);
    };

    Exps.prototype.LastObjectId = function (ret) {
        ret.set_string(parseResultObjId);
    };


    Exps.prototype.UserColumnData = function (ret, attribute) {
        var user = Parse["User"]["current"]();
        if (user) {
            var value = user["get"](attribute);
            ret.set_any(value);
        } else {
            console.warn("Trying to get Parse User Data without a Parse User. Need to Authenticate user first!");
            ret.set_string("");
        }
    };

    // @Deprecated
    Exps.prototype.UserAttribute = function (ret, attribute) {
        var user = Parse["User"]["current"]();
        if (user) {
            var value = user["get"](attribute);
            ret.set_any(value);
        } else {
            console.warn("Trying to get Parse User Data without a Parse User. Need to Authenticate user first!");
            ret.set_string("");
        }
    };

    Exps.prototype.QueryData = function (ret, idx, attribute) {
        if (queryData.length === 0 || idx > queryData.length - 1) {
            console.warn("QueryData index overflow: " + idx);
            return;
        }

        var value = queryData[idx].get(attribute);
        ret.set_any(value);
    };

    // @Deprecated
    Exps.prototype.QueryDataLength = function (ret) {
        var value = queryData.length;
        ret.set_int(value);
    };

    Exps.prototype.QueryRowCount = function (ret) {
        ret.set_int(queryRowCount);
    };

    Exps.prototype.QueryDataIndex = function (ret) {
        ret.set_int(queryDataIndex);
    };

    Exps.prototype.QueryDataCurrent = function (ret, attribute) {
        var value = queryData[queryDataIndex].get(attribute);
        ret.set_any(value);
    };

    Exps.prototype.FileObjectUrl = function (ret) {
        var value = fileObject["url"] ? fileObject["url"]() : "";
        ret.set_any(value);
    };

    Exps.prototype.FileObjectName = function (ret) {
        var value = fileObject["name"] ? fileObject["name"]() : "";
        ret.set_any(value);
    };

    //@Deprecated
    Exps.prototype.PushDataValue = function (ret, attribute) {
        var value = parsePushData[attribute];
        ret.set_any(value);
    };

    Exps.prototype.PushDataAlert = function (ret) {
        ret.set_string(parsePushDataAlert);
    };

    Exps.prototype.PushDataJSON = function (ret) {
        var value = JSON.stringify(parsePushDataJSON);
        ret.set_string(value);
    };

    Exps.prototype.PushInstallationId = function (ret) {
        ret.set_string(parseInstallationId);
    };


    pluginProto.exps = new Exps();

}
());

